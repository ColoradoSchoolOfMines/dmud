package main

import (
	"fmt"
	"strings"
	"github.com/rivo/tview"
)

var (
	app *tview.Application
	root *tview.Flex

	camelot = Place{name: "Camelot", description: "Tis a silly place.", moves: make([]Move, 0),
		dwellerEnters: func(d *Dweller) {}}
	antioch = Place{name: "Antioch", description: "Watch out for the rabbit.", moves: make([]Move, 0),
		dwellerEnters: func(d *Dweller) {}}
	anthrax = Place{name: "Anthrax", description: "Tis very perilous.", moves: make([]Move, 0),
		dwellerEnters: func(d *Dweller) {}}
	bridge  = Place{name: "the Bridge of Death", description: "Answer carefully.",
		dwellerEnters: func(d *Dweller) {
			death := tview.NewTextView().SetText("You are cast under the bridge and die a quick but painful death")
			form := tview.NewForm().
				AddInputField("What is your name?", "", 20, nil, nil).
				AddInputField("What is your quest?", "", 20, nil, nil).
				AddInputField("What is the capital of Assyria?", "", 20, nil, nil).
				AddButton("Submit",
				func() {
					app.SetRoot(death, true)
				})
			form.SetBorder(true).SetTitle("Answer the five (three) questions as best as you can")
			app.SetRoot(form, true)
		}}

	player = Dweller{name: "John Cleese", health: 14, location: &camelot}
)

func setupPlaces() {
	camelot.AddMove(Move{description: "Travel northward", key: 'n', place: &antioch})
	camelot.AddMove(Move{description: "Travel southward", key: 's', place: &bridge})
	camelot.AddMove(Move{description: "Travel westward",  key: 'w', place: &anthrax})
	antioch.AddMove(Move{description: "Travel southward", key: 's', place: &camelot})
	anthrax.AddMove(Move{description: "Travel eastward",  key: 'e', place: &camelot})
}

func main() {
	setupPlaces()

	app = tview.NewApplication()
	stats := tview.NewTextView().SetDynamicColors(true).SetRegions(true)
	body := tview.NewTextView()
	actions := tview.NewList()

	root = tview.NewFlex().SetDirection(tview.FlexRow).
		AddItem(stats, 4, 1, false).
		AddItem(body, 0, 1, false).
		AddItem(actions, 5, 1, true)

	var update func()
	update = func() {
		body.Clear()
		fmt.Fprintf(body, "You are in %s. %s", player.location.name, player.location.description)
		actions.Clear()

		move_func := func(p *Place) func() {
			return func() {
				player.location = p
				player.location.dwellerEnters(&player)
				update()
			}
		}

		for _, m := range player.location.moves {
			actions.AddItem(m.description, fmt.Sprintf("-> %s", m.place.name),
				m.key, move_func(m.place))
		}
	}
	update()

	stats.SetBorder(true)
	stats.SetTitle("Stats")
	fmt.Fprintf(stats, "Health: [:red]%s[:black] (%d)", strings.Repeat(" ", int(player.health)), player.health)

	if err := app.SetRoot(root, true).SetFocus(root).Run(); err != nil {
		panic(err)
	}
}
