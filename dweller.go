package main


type Dweller struct {
	name        string
	description string
	location    *Place
	speed       uint
	health      uint
	attack      uint
	defense     uint
	special     uint
}
