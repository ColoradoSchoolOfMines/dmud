package main


import "github.com/deckarep/golang-set"


type Move struct {
	key          rune
	description  string
	place        *Place
}


type Place struct {
	name           string
	description    string
	moves          []Move
	items          mapset.Set
	dwellerEnters  func(d *Dweller)
}


func (p *Place) AddMove(move Move) {
	left := 0
	right := len(p.moves)

	for left < right {
		i := (right-left)/2
		if move.key > p.moves[i].key {
			left = i+1
		} else {
			right = i
		}
	}

	p.moves = append(p.moves, Move{})
	copy(p.moves[left+1:], p.moves[left:])
	p.moves[left] = move
}
