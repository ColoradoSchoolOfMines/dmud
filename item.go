package main

type UseItem func(map[Dweller]interface{})

type Item struct {
	name        string
	description string
	// use         map[UseItem]interface{}
}
