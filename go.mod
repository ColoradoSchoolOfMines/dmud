module gitlab.com/ColoradoSchoolOfMines/dmud

go 1.12

require (
	github.com/deckarep/golang-set v1.7.1
	github.com/rivo/tview v0.0.0-20190629194509-17ae69181737
)
